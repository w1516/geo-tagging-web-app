// File origin: VS1LAB A2
// testtest
/* eslint-disable no-unused-vars */

// const { response } = require("express");
// const response = require("express");


// This script is executed when the browser loads index.html.

// "console.log" writes to the browser's console. 
// The console window must be opened explicitly in the browser.
// Try to find this output in the browser...
console.log("The geoTagging script is going to start...");

let clientTags = [];
let mapManager;
/**
 * TODO: 'updateLocation'
 * A function to retrieve the current location and update the page.
 * It is called once the page has been fully loaded.
 */
 function updateLocation() {
    let tagLat = document.getElementById("latitude_input")
    let tagLong = document.getElementById("longitude_input")
    //console.log(tagLat);
    //console.log(tagLong);
    let mapManager = new MapManager("EHhsx33AjvAofKoU9zPL366HAQnvBAwE");

    let mapView = document.querySelector("#mapView");
    let listElements = JSON.parse(mapView.getAttribute("data-tags"));
    console.log(listElements);

    var mapURL = mapManager.getMapUrl(
        document.getElementById("latitude_input").value, 
        document.getElementById("longitude_input").value,
        listElements
    )
    document.getElementById("mapView").setAttribute("src",mapURL);

    // Erweitert um Abfrage
    if (tagLat.value == null || tagLong.value == null ||
        tagLat.value === "" || tagLong.value === "") {
            console.log("it worked");
        try {
            LocationHelper.findLocation(updateDocument);
        } catch (e) {
            console.log("The GeoLocation API is currently unavailable.");
        }
    }
}

function updateDocument(helper) {
    console.log("in function updateDocument");

    document.getElementById("latitude_input").value = helper.latitude; 
    document.getElementById("longitude_input").value = helper.longitude;
    document.getElementById("discovery_latitude").value = helper.latitude;
    document.getElementById("discovery_longitude").value = helper.longitude;

    let mapManager = new MapManager("EHhsx33AjvAofKoU9zPL366HAQnvBAwE");
    let tagList = document.getElementById("mapView").dataset.tags;

    console.log("Taglist Log:" + tagList);

    let mapURL = mapManager.getMapUrl(
        helper.latitude,
        helper.longitude,
        JSON.parse(tagList)
    );
    // document.getElementById("mapView").src = mapURL;
}

// Wait for the page to fully load its DOM content, then call updateLocation
document.addEventListener("DOMContentLoaded", () => {
    updateLocation();
    getInitialValues();
})

function getInitialValues() {
    let inputData = {
        latitude: document.getElementById("latitude_input").value,
        longitude: document.getElementById("longitude_input").value,
        name: document.getElementById("name_input").value,
        hashtag: document.getElementById("hashtag_input").value
    }

    fetch("/api/geotags", {
        method: "GET"
    })
        .then(response => response.json())
        .then(data => createAfterSearchButton(data, inputData.latitude, inputData.longitude))
        .catch(error => console.log('Error: ' + error))
};

    document.getElementById("add_tag_button").addEventListener("click", async function (){
        let inputData = {
            latitude: document.getElementById("latitude_input").value,
            longitude: document.getElementById("longitude_input").value,
            name: document.getElementById("name_input").value,
            hashtag: document.getElementById("hashtag_input").value
        }
        if(!(inputData.hashtag.match(/#[a-zA-Z0-9]+/))){
            return;
        }

        fetch("api/geotags", {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(inputData)
        })
        .then(response => response.json())
        .then(data => {
            let element = document.createElement("li")
            element.innerHTML = (`${data.name} ${data.latitude} ${data.longitude} ${data.hashtag}`);
            document.getElementById("discoveryResults").appendChild(element);
            clientTags.push(data);
            // console.log("In onAddButtonSuccess" + dataJson);
            adjustMap(clientTags, inputData.latitude, inputData.longitude);
        })
        .catch(error => console.log(error));
    });

    document.getElementById("search_button").addEventListener("click", async function () {
        let searchWord = document.getElementById("search_input").value.toLowerCase();
        let inputData = {
            latitude: document.getElementById("latitude_input").value,
            longitude: document.getElementById("longitude_input").value,
            name: document.getElementById("name_input").value,
            hashtag: document.getElementById("hashtag_input").value
        }

        let getURL = "/api/geotags";
        let searchString = "?search=" + searchWord + "&lat=" + inputData.latitude + "&lon=" + inputData.longitude;

        if (searchWord.length > 0) {
            getURL = getURL + searchString;
        }

           fetch(getURL, {
                method: 'GET',
            })
            .then(response => response.json())
            .then(data => createAfterSearchButton(data, inputData.latitude, inputData.longitude))
            .catch(error => console.log(error));

    })

    // Helper Function for Search Button Event
    function createAfterSearchButton(data, lat, lon) {
        //console.log("Searching: " + data.geotags);
        document.getElementById("discoveryResults").innerHTML = "";
        for (let i = 0; i < data.geotags.length; i++) {
            console.log("GeoTag: " + data.geotags[i].hashtag);
            let element = document.createElement("li")
            element.innerHTML = (`${data.geotags[i].name} ${data.geotags[i].latitude} ${data.geotags[i].longitude} ${data.geotags[i].hashtag}`);
            document.getElementById("discoveryResults").appendChild(element);
        }
        clientTags = data.geotags.slice();
        adjustMap(clientTags, lat, lon);
    }

    function adjustMap (data, lat, lon) {
        let newMap = document.createElement("img");
        newMap.setAttribute("data-tags", JSON.stringify(data));
        newMap.setAttribute("id", "mapView");
        newMap.setAttribute("alt", "new locations");
        newMap.setAttribute("src", mapManager.getMapUrl(lat, lon, data));
        console.log(newMap);
        document.getElementById("mapView").replaceWith(newMap);
    }