# Geo Tagging Web-App

Implementing a best-practice modern web app with the following technologies:
- HTML
- CSS 
- JS 
- Node.js 
- Express  
- EJS 
- AJAX 
- REST

# Setup

1. Go to the root directory of the project and run `npm install`
2. In the same directory run `npm start`
3. In a Browser search http://localhost:3000 and have fun trying out the app

# Demo

![App View](https://gitlab.com/w1516/geo-tagging-web-app/-/raw/main/res/demo.png)
